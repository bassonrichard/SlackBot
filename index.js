'use strict';

var express = require('express');
var app = express();

var bodyParser = require('body-parser');
var router = express.Router();
var http = require('http').Server(app);
var cleverbot = require("cleverbot.io");

var port = process.env.PORT || 8080;
var host = process.env.URL;

require('dotenv').config()

cleverbot = new cleverbot(process.env.CLEVER_BOT_USER, process.env.CLEVER_BOT_KEY);
cleverbot.setNick("replybot");
cleverbot.create(function (err, session) {
    if (err) {
        console.log('cleverbot create fail.');
    } else {
        console.log('cleverbot create success.');
    }
});

// You need it to get the body attribute in the request object.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}))


var botkit = require('botkit');

var slackController = botkit.slackbot({
  clientId: process.env.SLACK_CLIENT_ID,
  clientSecret: process.env.SLACK_CLIENT_SECRET,
  scopes: ['bot'],
  redirectUri: process.env.URL + '/oauth',
  json_file_store: __dirname + '/.data/db/',
  disable_startup_messages: true,
  hostname: process.env.URL,
  host: port
});

slackController.startTicking();
slackController.createOauthEndpoints(app);
slackController.createWebhookEndpoints(app);

slackController.hears(['.*'], ['ambient', 'direct_message','direct_mention','mention'],function(bot,message) {

  console.log("Slackbot received a message with text -  "+message.text);

  var msg = message.text;
  cleverbot.ask(msg, function (err, response) {
      if (!err) {
          bot.reply(message, response);
      } else {
          console.log('cleverbot err: ' + err);
      }
  });
});

slackController.hears('lunch','direct_message,direct_mention',function(bot,message) {
  bot.reply(message,"WOOO~ Let's eat!!");
});

slackController.hears('Interactive', 'direct_message', function(bot, message) {

  bot.reply(message, {
      attachments:[
          {
              title: 'Do you want to interact with my buttons? :wink:',
              callback_id: '123',
              attachment_type: 'default',
              actions: [
                  {
                      "name":"dialog",
                      "text": "Yes",
                      "value": "yes",
                      "type": "button",
                  },
                  {
                      "name":"no",
                      "text": "No",
                      "value": "no",
                      "type": "button",
                  }
              ]
          }
      ]
  });
});

slackController.on('interactive_message_callback', function(bot, trigger) {

  // is the name of the clicked button "dialog?"
  if (trigger.actions[0].name.match(/^dialog/)) {

        var dialog =bot.createDialog(
              'Title of dialog',
              '123',
              'Submit'
            ).addText('Text','text','some text')
              .addSelect('Select','select',null,[{label:'Foo',value:'foo'},{label:'Bar',value:'bar'}],{placeholder: 'Select One'})
             .addTextarea('Textarea','textarea','some longer text',{placeholder: 'Put words here'})
             .addNumber('Number','Number')
             .addUrl('Website','url','http://botkit.ai');


        bot.replyWithDialog(trigger, dialog.asObject(), function(err, res) {
         //error handeling
        });

    }

});


slackController.on('dialog_submission', function(bot, message) {
  var submission = message.submission;
  bot.reply(message, 'Got it!');
  bot.dialogOk();
  bot.dialogClose();
});

app.listen(port);
console.log('Listening on port ... '+port);
